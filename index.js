var Ajv = require('ajv');
var ajv = new Ajv({useDefaults: true, removeAdditional: 'all'});

module.exports = function (plugin_name, plugin_settings_schema) {
    this.plugin_name = plugin_name;
    this.settings_schema = plugin_settings_schema;
    validate_settings_schema(plugin_settings_schema);

    function validate_settings_schema(settings_schema) {

        try {
            this.validate_settings = ajv.compile(settings_schema);
            this.settings_schema = settings_schema;
        } catch (ex) {
            this.validate_settings = null;
            this.settings_schema = null;
        }

    }

    function load(settings = {}) {
        if (this.validate_settings) {
            let settings_validated = this.validate_settings(settings);
            if (settings_validated) {
                this.settings = settings;
            } else {
                throw new Error(this.name + " is unable to load settings.");
            }
        } else {
            throw new Error(this.name + " is unable to validate settings. Check is plugin settings schema valid.");
    }
    }

    function isValidSettings() {
        if (this.validate_settings) {
            if (settings) {
                let settings_validated = this.validate_settings(settings);
                if (settings_validated) {
                    return settings_validated;
                } else {
                    throw new Error(this.name + " is unable to load settings.");
                }
            }else{
                return false;
            }
        } else {
            throw new Error(this.name + " is unable to validate settings. Check is plugin settings schema valid.");
        }
    }



    function runAction(data, callback) {
        if (!data || !callback) {
            return callback('Insert data function must have data and callback parameters');
        }

        this.executeAction(data, function (error, result) {
            if (!error) {
                //console.log('insert result type: ', typeof result);
                if (typeof result === 'object') {
                    return callback(null, result);
                }
                return callback('executeAction need to return object');
            }
            return callback(error);
        });
    }

    function executeAction(data, callback) {
        callback('Plugin must implement action method');
    }

    function runAction(data, callback) {
        if (!data || !callback) {
            return callback('Insert data function must have data and callback parameters');
        }

        this.executeAction(data, function (error, result) {
            if (!error) {
                console.log('insert result type: ', typeof result);
                if (typeof result === 'object') {
                    return callback(null, result);
                }
                return callback('executeAction need to return object');
            }
            return callback(error);
        });
    }

    function getName() {
        return this.name;
    }

    return {
        name: this.plugin_name,
        settings_schema: this.settings_schema,
        validate_settings: this.validate_settings,
        settings: this.settings,
        isValidSettings: isValidSettings,
        phase: 'init',
        load: load,
        action: runAction,
        executeAction: executeAction
    };
};