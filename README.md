# Base plugin for apicloak

We use this plugin to compose new plugins. It is start base.

Now we will create simple plugin that will write some message to console.log. Lets start.

First create new folder with name structured like **[name_of_plugin].apicloak.plugin**. For example:
```javascript
shout.apicloak.plugin
```
After that open command line and position it to newly created folder. When we are positioned in folder run command to init nodejs project:
```
npm init -y
```
(-y options init project with default init options)

Then install base plugin:
```
npm install -S base.apicloak.plugin
```

After installed base plugin create **index.js** and **settings.schema.js** empty files.

Now folder structure look like:
```
shout.apicloak.plugin:
|	index.js
| 	package.json
|	settings.schema.js
```

Open **settings.schema.js** file and define allowed plugin settings(validation is done by [ajv JSON validator](https://github.com/epoberezkin/ajv) ). For shout plugin we will define msg setting that must be string and if not provided default message will be generated "This is test shout message!!!". Shout **settings.schema.js** look like:
```javascript
// settings.schema.js
const schema = {
    properties: {
        msg: {type: 'string', default:'This is test shout message!!!'}
    },
    required:['msg']
};

module.exports = schema;
```

Next we go on **index.js**. Open this file and add requires for base plugin and settings schema: 
```javascript
// index.js
const base = require('base.apicloak.plugin');
const settings_schema = require('./settings.schema.js');
```

For creating plugin we will object composition. Shout function will be composed from base plugin. Using `Object.assign` we create new shout plugin object like this:
```javascript
// index.js
const base = require('base.apicloak.plugin');
const settings_schema = require('./settings.schema.js');

module.exports = function () {
    return Object.assign({}, base('shout', settings_schema), { //first argument is plugin name
        //later we add action here
    });
};
```

To make newly created plugin work we need to define *executeAction* method. For shout plugin we will write message defined in settings to console. Shout *executeAction* method look like:
```javascript
// index.js
const base = require('base.apicloak.plugin');
const settings_schema = require('./settings.schema.js');

module.exports = function () {
    return Object.assign({}, base('shout', settings_schema), {
        executeAction: function (data, callback) {
            console.log("SHOUTING MESSAGE: ", this.settings.msg);

            callback(null, data);
        }
    });
};
```

As you see *executeAction* method have arguments:
* data - is data from already executed request phases,
* callback - as usual javascript callback.

Mehod execute must allways return *callback* with parameters err and data like so:
```
callback(err,data)
```
**It's prefered that data parameter is always incremental**(always add new information to data parameter and try to never remove any exiting information  if not necessary)

After tested plugin it can be published to npm and it is ready for use. To add to npm use command: 
```
npm publish
```
[More about npm publishing package](https://docs.npmjs.com/getting-started/publishing-npm-packages) 