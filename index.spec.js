const chai = require('chai');
const expect = chai.expect;
const should = chai.should;
const assert = chai.assert;
const base = require('./index.js');

describe('base.apicloak.plugin', function () {
    describe('plugin composed of it', function () {
        let plugin_name = 'base_composed_plugin';
        let base_composed_plugin;
        let error = null;
        
        before(function (done) {
            let plugin_settings_schema = {
                type: 'object',
                properties: {
                    name: {type: 'string', default: 'Test name'},
                    msg: {type: 'string', default: 'Test msg'}
                },
                required: ['name', 'msg']
            };
            
            try {

            } catch (ex) {
                error = ex;
            }
            base_composed_plugin = Object.assign({}, base(plugin_name, plugin_settings_schema));
            done();
        });

        it('should check that plugin settings schema is valid', function () {
            expect(error).to.be.null;
        });

    });
    
    describe('load function', function () {
        
        let plugin_name = 'base_composed_plugin';
        let base_composed_plugin;
        let error = null;
        
        before(function (done) {
            let plugin_settings_schema = {
                type: 'object',
                properties: {
                    name: {type: 'string', default: 'Test name'},
                    msg: {type: 'string', default: 'Test msg'}
                },
                required: ['name', 'msg']
            };
            
            try {

            } catch (ex) {
                error = ex;
            }
            base_composed_plugin = Object.assign({}, base(plugin_name, plugin_settings_schema));
            done();
        });
        
        it('should load() function return no errors', function () {
            let settings = {
                name: 'Bootta',
                test: 'test_val'
            };
            let error = null;
            try {
                base_composed_plugin.load(settings);
            } catch (ex) {
                error = ex;
            }

            expect(error).to.be.null;
        });
    });



});